package nz.ac.vuw.jmkr.reporting;

import nz.ac.vuw.jmkr.rt.MarkingResultRecord;
import java.util.List;

/**
 * Reporter interface.
 * @author jens dietrich
 */
public interface Reporter {
    void generateReport (List<MarkingResultRecord> results);
}
